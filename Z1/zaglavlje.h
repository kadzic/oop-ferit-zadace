#ifndef HEADER_H
#define HEADER_H

#include <iostream>
#include <time.h>
#include <stdlib.h>



class Episode{
    public:
        int NumberOfWatchers = 0;
        float SumScores;
        float MaxScore;

        Episode();
        Episode(int, float, float);

        void addView(float);
        int getViewerCount();
        float getMaxScore();
        float getAverageScore();
};

float generateRandomScore();

#endif // HEADER_H
