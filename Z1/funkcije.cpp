#include "zaglavlje.h"


Episode::Episode(){
}

Episode::Episode(int x, float y, float z){
    NumberOfWatchers = x;
    SumScores = y;
    MaxScore = z;

}

void Episode::addView(float random){
    NumberOfWatchers++;
    SumScores = SumScores + random;

    if(MaxScore < random){
        MaxScore = random;
    }
}

int Episode::getViewerCount(){
    return NumberOfWatchers;
}

float Episode::getMaxScore(){
    return MaxScore;
}

float Episode::getAverageScore(){
    float a = (float)SumScores / (float)NumberOfWatchers;
    return a;
}
